package services;

import com.google.inject.ImplementedBy;
import models.FlashCard;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

@ImplementedBy(DefaultFlashCardService.class)
public interface FlashCardService {
    /**
     * Return's list of all flashcards.
     * @return list of all flashcards
     */
    CompletionStage<Stream<FlashCard>> get();

    /**
     * Returns flashcard with given identifier.
     * @param id flashcard identifier
     * @return flashcard with given identifier or {@code null}
     */
    CompletionStage<FlashCard> get(final Long id);

    /**
     * Removes flashcard with given identifier.
     * @param id flashcard identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Long id);

    /**
     * Updates flashcard with given identifier.
     * @param updatedFlashCard flashcard with updated fields
     * @return updated flashcard
     */
    CompletionStage<FlashCard> update(final FlashCard updatedFlashCard);

    /**
     * Adds the given flashcard.
     * @param flashCard to add
     * @return added flashcard
     */
    CompletionStage<FlashCard> add(final FlashCard flashCard);

    /**
     * Return's list of all Flashcard-Sets.
     * @return list of all flashcard-sets
     */
    CompletionStage<Stream<String>> getSets();

    /**
     * Return's list of all Topics.
     * @return list of all topics
     */
    CompletionStage<Stream<String>> getTopics(final String q);
}
