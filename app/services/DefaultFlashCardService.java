package services;

import models.FlashCard;
import models.FlashCardRepository;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;


public class DefaultFlashCardService implements FlashCardService {

    private FlashCardRepository flashCardRepository;

    @Inject
    public DefaultFlashCardService(FlashCardRepository flashCardRepository) {
        this.flashCardRepository = flashCardRepository;
    }

    /**
     * Return's list of all flashcards.
     * @return list of all flashcards
     */
    public CompletionStage<Stream<FlashCard>> get() {
        return flashCardRepository.list();
    }

    /**
     * Return's list of all Flashcard-Sets.
     * @return list of all flashcard-sets
     */
    public CompletionStage<Stream<String>> getSets() {
        return flashCardRepository.getSets();
    }

    /**
     * Return's list of all Topics.
     *  @param q category
     * @return list of all topics
     */
    public CompletionStage<Stream<String>> getTopics(String q) {
        return flashCardRepository.getTopics(q);
    }

    /**
     * Returns flashcard with given identifier.
     * @param id flashcard identifier
     * @return flashcard with given identifier or {@code null}
     */
    public CompletionStage<FlashCard> get(Long id) {
        return flashCardRepository.find(id);
    }

    /**
     * Removes flashcard with given identifier.
     * @param id flashcard identifier
     * @return {@code true} on success  {@code false} on failure
     */
    public CompletionStage<Boolean> delete(Long id) {
        return flashCardRepository.remove(id);
    }

    /**
     * Updates flashcard with given identifier.
     * @param updatedFlashCard flashcard with updated fields
     * @return updated flashcard
     */
    public CompletionStage<FlashCard> update(FlashCard flashCard) {
        return flashCardRepository.update(flashCard);
    }

    /**
     * Adds the given flashcard.
     * @param flashCard to add
     * @return added flashcard
     */
    public CompletionStage<FlashCard> add(FlashCard flashCard) {
        return flashCardRepository.add(flashCard);
    }


}
