package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name = "flashcard")
public class FlashCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    //Datafields
    private long id;
    private String topic;
    private String category;
    private String front;
    private String back;

    //Methods get&set
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
    public String getTopic() { return topic; }
    public void setTopic(String topic) { this.topic = topic; }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) { this.category = category; }
    public String getFront() { return front; }
    public void setFront(String front) { this.front = front; }
    public String getBack() { return back; }
    public void setBack(String back) { this.back = back; }
}
