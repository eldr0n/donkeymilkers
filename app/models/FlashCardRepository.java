package models;

import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class FlashCardRepository {
    private final JPAApi jpaApi;

    @Inject
    public FlashCardRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }
    public CompletionStage<FlashCard> add(FlashCard flashCard) { return supplyAsync(() -> wrap(em -> insert(em, flashCard))); }
    public CompletionStage<FlashCard> update(FlashCard flashCard) { return supplyAsync(() -> wrap(em -> update(em, flashCard))); }
    public CompletionStage<FlashCard> find(Long id) { return supplyAsync(() -> wrap(em -> find(em, id))); }
    public CompletionStage<Boolean> remove(Long id) {
        return supplyAsync(() -> wrap(em -> remove(em, id)));
    }
    public CompletionStage<Stream<FlashCard>> list() {
        return supplyAsync(() -> wrap(this::list));
    }
    public CompletionStage<Stream<String>> getSets() {
        return supplyAsync(() -> wrap(this::getSets));
    }
    public CompletionStage<Stream<String>> getTopics(String q) { return supplyAsync(() -> wrap(em -> getTopics(q, em))); }
    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    /**
     * Inserts the given flashCard
     * @param flashCard to insert
     * @return inserted flashCard
     */
    private FlashCard insert(EntityManager em, FlashCard flashCard) {
        em.persist(flashCard);
        return flashCard;
    }

    /**
     * Updates the given flashcard
     * @param flashCard with new parameters
     * @return updatedFlashcard
     */
    private FlashCard update(EntityManager em, FlashCard flashCard) {
        FlashCard flashCardToUpdate = em.find(FlashCard.class, flashCard.getId());
        flashCardToUpdate.setTopic(flashCard.getTopic());
        flashCardToUpdate.setCategory(flashCard.getCategory());
        flashCardToUpdate.setFront(flashCard.getFront());
        flashCardToUpdate.setBack(flashCard.getBack());
        return flashCardToUpdate;
    }

    /**
     * Returns Flashcard with given identifier
     * @param id flashcard identifier
     * @return flashcard
     */
    private FlashCard find(EntityManager em, Long id) {
        return em.find(FlashCard.class, id);
    }

    /**
     * Removes flashcard with given identifier.
     * @param id flashcard identifier
     * @return {@code true} on success  {@code false} on failure
     */
    private Boolean remove(EntityManager em, Long id) {
        FlashCard flashCard = em.find(FlashCard.class, id);
        if (null != flashCard) {
            em.remove(flashCard);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return's a list of all flashcards
     * @return list of all falshcards
     */
    private Stream<FlashCard> list(EntityManager em) {
        List<FlashCard> flashCards = em.createQuery("select f from flashcard f", FlashCard.class)
                .getResultList();
        return flashCards.stream();
    }

    /**
     * Return's a list of all Sets
     * @return list of all Sets
     */
    private Stream<String> getSets(EntityManager em) {
        List<String> Sets = em.createQuery("select category from flashcard group by category", String.class)
                .getResultList();
        return Sets.stream();
    }

    /**
     * Return's a list all the Topics of a category
     * @param q category
     * @return list of topics to a given caregory
     */
    private Stream<String> getTopics(String q, EntityManager em) {
        List<String> Sets = em.createQuery("select topic from flashcard where category = :cat group by topic", String.class)
                .setParameter("cat", q)
                .getResultList();
        return Sets.stream();
    }
}
