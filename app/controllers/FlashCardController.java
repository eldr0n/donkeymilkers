package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.FlashCard;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.FlashCardService;
import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class FlashCardController extends Controller {
    private final FlashCardService flashCardService;


    @Inject
    public FlashCardController(FlashCardService flashCardService) {
        this.flashCardService = flashCardService;
    }

    public CompletionStage<Result> flashcards(String q) {
        return flashCardService.get().thenApplyAsync(cardStream -> ok(Json.toJson(cardStream.collect(Collectors.toList()))));
    }


    public CompletionStage<Result> get(long id) {
        return flashCardService.get(id).thenApplyAsync(card -> ok(Json.toJson(card)));
    }

    public CompletionStage<Result> add(Http.Request request) {
        JsonNode jsonCard = request.body().asJson();
        FlashCard newCard = Json.fromJson(jsonCard, FlashCard.class);
        return flashCardService.add(newCard).thenApplyAsync(card -> ok(Json.toJson(card)));
    }

    public CompletionStage<Result> update(Http.Request request, long id) {
        JsonNode jsonCard = request.body().asJson();
        FlashCard updatedCard = Json.fromJson(jsonCard, FlashCard.class);
        updatedCard.setId(id);
        return flashCardService.update(updatedCard).thenApplyAsync(card -> ok(Json.toJson(card)));
    }

    public CompletionStage<Result> delete(long id) {
        return flashCardService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }

    public CompletionStage<Result> getSets(){
        return flashCardService.getSets().thenApplyAsync(cardStream -> ok(Json.toJson(cardStream.collect(Collectors.toList()))));
    }

    public CompletionStage<Result> getTopics(String q){
        return flashCardService.getTopics(q).thenApplyAsync(cardStream -> ok(Json.toJson(cardStream.collect(Collectors.toList()))));
    }
}
