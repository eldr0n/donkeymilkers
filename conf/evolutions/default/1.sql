# FlashCard schema

# --- !Ups
create table flashcard (
                           id bigint(20) NOT NULL AUTO_INCREMENT,
                           topic varchar(50),
                           category varchar(50),
                           front varchar(50),
                           back varchar(50),
                           PRIMARY KEY (id)

);

# --- !Downs
drop table flashcard;