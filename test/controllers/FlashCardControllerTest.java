package controllers;

import static play.test.Helpers.*;

import models.FlashCard;
import models.FlashCardRepository;
import org.junit.Test;
import org.mockito.Mockito;
import play.Application;
import play.libs.Json;
import play.inject.guice.GuiceApplicationBuilder;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;
import services.DefaultFlashCardService;
import services.FlashCardService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class FlashCardControllerTest extends WithApplication {
    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }


    @Test
    public void testDb() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/api/flashcards");
        Result result = route(app, request);
        assertEquals(OK, result.status());
    }

    @Test
    public void testParse() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/api/flashcards/1");
        Result result = route(app, request);
        assertEquals(OK, result.status());

        String expectedTopic = "";
        String expectedCategory = "";
        String expectedFront = "";
        String expectedBack = "";

        FlashCard flashCard = Json.fromJson(Json.parse(contentAsString(result)), FlashCard.class);
        assertEquals(expectedTopic, flashCard.getTopic());
        assertEquals(expectedCategory, flashCard.getCategory());
        assertEquals(expectedFront, flashCard.getFront());
        assertEquals(expectedBack, flashCard.getBack());
    }

    @Test
    public void testMock() {
        CompletionStage<FlashCard> future = CompletableFuture.supplyAsync(() -> new FlashCard());

        FlashCardRepository repositoryMock = Mockito.mock(FlashCardRepository.class);
        Mockito.when(repositoryMock.find(any(Long.class))).thenReturn(future);

        Long flashcardID = new Long(1);
        FlashCardService flashCardService = new DefaultFlashCardService(repositoryMock);
        flashCardService.get(flashcardID);

        verify(repositoryMock).find(flashcardID);

    }
}
