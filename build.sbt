name := """donkeymilkers"""
organization := "ch.fhgr"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies += guice
libraryDependencies += "org.mockito" % "mockito-core" % "2.10.0" % "test"
libraryDependencies ++= Seq(
  javaJdbc,
  "com.h2database" % "h2" % "1.4.192",
  evolutions,
  javaJpa,
  "org.hibernate" % "hibernate-core" % "5.4.9.Final",
)
herokuAppName in Compile := "donkeymilkers"
herokuJdkVersion in Compile := "14"