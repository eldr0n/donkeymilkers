import React from "react";
import "./set.css"
import Topics from "../topics/Topics.jsx"


class Set extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showTopics: false,
        };
        this.handleToggleClick = this.handleToggleClick.bind(this);
    }

    //update state onClick
    handleToggleClick() {
        this.setState({showTopics: !this.state.showTopics});
    }

    render() {
        if (this.state.showTopics) {
            return (
                <div className="card" onClick={() => this.handleToggleClick()}>
                    <p>{this.props.set}</p>
                    {console.log(this.state.showTopics)}
                    <Topics key={this.props.set} set={this.props.set}/>
                </div>
                )
        }
        return (
            <div className="card" onClick={() => this.handleToggleClick()}>
                <p>{this.props.set}</p>
                {console.log(this.state.showTopics)}
            </div>
        );
    }
}

export default Set;