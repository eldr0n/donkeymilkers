import React from "react";
import Set from "./Set.jsx";
import index from "../index.css";

class Sets extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            sets: []
        };
    }

    //Get Sets from database
    componentDidMount(){
        fetch('/api/sets ')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    sets: data
                });
            });
    }
    render(){
        return(
          <section>
              {
                  this.state.sets.length > 0 ?
                      this.state.sets.map(function(set){
                          return(
                              <Set key={set} set={set}/>
                          );

                      }): <h1>No Sets available</h1>

              }
          </section>
        );
    }
}

export default Sets;