import React from "react";
import "./flashcard.css"
import {Link, withRouter} from 'react-router-dom'
import {toast} from "react-toastify";

class Flashcard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showCard: true
        };
        this.deleteCard = this.deleteCard.bind(this);

    }

    //deletes the flashCard with the given id from the database
    deleteCard(id) {
        fetch('/api/flashcards/' + id, {
            method: 'delete',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(response => {
            //response success / error
            if (response.status !== 200) {
                toast.error('Looks like there was a problem', {
                    position: "bottom-center",
                });
            } else {
                toast('🦄 Flashcard deleted', {
                    position: "bottom-center",
                });
                this.setState({
                    showCard: false
                });
                this.props.cardDeleted()
            }
        })
    }


    render() {
        if (this.state.showCard) {
            return (
                <section className="flashcard">
                    <div className="flip-card">
                        <div className="flip-card-inner">
                            <div className="flip-card-front">
                                <p>{this.props.card.front}</p>
                            </div>
                            <div className="flip-card-back">
                                <p>{this.props.card.back}</p>
                            </div>
                        </div>
                    </div>
                    <div className="edit">
                    <Link to={"/flashcards/edit/" + this.props.card.id}><i className="fas fa-edit"/></Link>
                    <p onClick={() => this.deleteCard(this.props.card.id)}><i className="fas fa-trash-alt"/></p>
                    </div>
                </section>
            );
        } else return null;
    }
}

export default withRouter(Flashcard);