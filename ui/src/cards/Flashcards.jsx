import React from "react";
import {Link, withRouter} from 'react-router-dom'
import Flashcard from "./Flashcard.jsx";
import '../test/test.css';

class Flashcards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            set: this.props.match.params.set,
            topic: this.props.match.params.topic,
            flashcards: [],
            disabled: false
        };
        this.filterCards = this.filterCards.bind(this);
        this.testYourself = this.testYourself.bind(this);
        this.deleted = this.deleted.bind(this);
        this.createCard = this.createCard.bind(this);
    }

    //Getting the list of all Flashcards
    componentDidMount() {
        fetch('/api/flashcards')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    flashcards: this.filterCards(data)
                });
            });
    }

    //filter flashcards by given topic & category
    filterCards(data) {
        let filtered = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].category === this.state.set &&
                data[i].topic === this.state.topic) {
                filtered.push(data[i])
            }
        }

        return filtered;
    }

    //redirect to test yourself
    testYourself() {
        let path = "/flashcards/" + this.state.set + "/" + this.state.topic + "/test";
        this.props.history.push(path);
    }

    //flashcard deleted
    deleted() {
        this.state.flashcards.length == 1 ? this.setState({disabled: true}) : this.setState({disabled: false})
    }

    createCard(flashcard) {
        return (
            <Flashcard key={flashcard.id} card={flashcard} cardDeleted={this.deleted}/>
        );
    }

    render() {
        return (
            <section>
                <button className="test-link" disabled={this.state.disabled} onClick={this.testYourself}>Test yourself
                </button>
                {
                    this.state.flashcards.length > 0 ?
                        this.state.flashcards.map(this.createCard) : null
                }
                {
                    this.state.disabled ? <h1>No Flashcards available</h1> : null
                }
            </section>
        );
    }
}

export default withRouter(Flashcards);