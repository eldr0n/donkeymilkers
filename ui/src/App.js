import React from 'react';
import './App.css';
import Sets from "./set/Sets.jsx";
import Flashcards from "./cards/Flashcards.jsx";
import Create from "./create/Create.jsx";
import Edit from "./edit/Edit.jsx";
import Testyourself from "./test/Testyourself.jsx";
import Home from "./Home";
import {ToastContainer} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

export default function App() {
    return (
        <div className="App">
            <ToastContainer/>
            <Router>
                <header className="App-header">
                    <nav>
                        <Link to="/">Home</Link>
                        <Link to="/sets"> Sets </Link>
                        <Link to="/create">Create</Link>
                    </nav>
                </header>
                <Switch>
                    <Route path="/flashcards/edit/:id" component={Edit}>
                    </Route>
                    <Route path="/flashcards/:set/:topic/test">
                        <Testyourself/>
                    </Route>
                    <Route path="/flashcards/:set/:topic">
                        <Flashcards />
                    </Route>
                    <Route path="/sets">
                        <Sets/>
                    </Route>
                    <Route path="/create">
                        <Create/>
                    </Route>
                    <Route path="/">
                        <Home/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

