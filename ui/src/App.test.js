import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders Sets link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText("Sets");
  expect(linkElement).toBeInTheDocument();
});
