import React from "react";
import {Link, withRouter} from 'react-router-dom'
import './test.css';
import {toast} from 'react-toastify';
import {Doughnut} from "react-chartjs-2";

class Testyourself extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            set: this.props.match.params.set,
            topic: this.props.match.params.topic,
            flashcards: [],
            front: "",
            back: "",
            backinput: "",
            index: "",
            length: "",
            correct: 0,
            incorrect: 0,
            toanswer: "",
            disabled: false,
            finished: false,
        };
        this.filterCards = this.filterCards.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.restart = this.restart.bind(this);
    }

    //Doughnut Settings
    syncDataset() {
        return {
            labels: [
                'Correct',
                'Incorrect',
                'Flashcards to answer '
            ],
            datasets: [{
                data: [this.state.correct, this.state.incorrect, this.state.toanswer],
                backgroundColor: [
                    '#228B22',
                    '#FA8072',
                    '#BEBEBE'
                ],
                hoverBackgroundColor: [
                    '#228B22',
                    '#FA8072',
                    '#BEBEBE'
                ]
            }]
        };
    }

    //Getting the list of all Flashcards
    componentDidMount() {
        fetch('/api/flashcards')
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    flashcards: this.filterCards(data)
                });
            });
    }

    //filter flashcards by given topic & category
    filterCards(data) {
        let filtered = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].category === this.state.set &&
                data[i].topic === this.state.topic) {
                filtered.push(data[i])
            }
        }
        //set state-data
        this.setState({index: 0});
        this.setState({toanswer: filtered.length});
        this.setState({length: filtered.length});
        this.setState({front: filtered[this.state.index].front});
        this.setState({back: filtered[this.state.index].back});

        return filtered;
    }

    //Check input
    handleSubmit(event) {
        event.preventDefault();

        //Input Correct / Incorrect
        if (this.state.back === this.state.backinput) {
            toast.success('🦄 Correct! 🦄', {
                position: "bottom-center",
                autoClose: 2000,
            });
            this.setState({correct: this.state.correct + 1});
            //this.setState(this.state.correct = this.state.correct + 1;)

        } else {
            toast.error('🦄 Incorrect! 🦄', {
                position: "bottom-center",
                autoClose: 2000,
            });
            this.setState({incorrect: this.state.incorrect + 1});
        }
        this.setState({toanswer: this.state.toanswer - 1});

        //quiz finished ? continue : stop quiz
        if (this.state.index + 1 < this.state.length) {
            this.setState({index: this.state.index + 1});
            this.setState({front: this.state.flashcards[this.state.index + 1].front});
            this.setState({back: this.state.flashcards[this.state.index + 1].back});
            this.setState({backinput: ""});
        } else {this.finished()}
    }

    //stop quiz
    finished(){
        this.setState({disabled: true});
        this.setState({finished: true});
        toast('🦄 Finished! 🦄 Correct: ' + this.state.correct + '/' + this.state.length, {
            position: "bottom-center",
            autoClose: 5000,
            closeOnClick: true,
            draggable: true,
        });
    }

    //restart quiz
    restart(){
        this.setState({index: 0});
        this.setState({toanswer: this.state.length});
        this.setState({incorrect: 0});
        this.setState({correct: 0});
        this.setState({front: this.state.flashcards[0].front});
        this.setState({back: this.state.flashcards[0].back});
        this.setState({backinput: ""});
        this.setState({finished: false});
        this.setState({disabled: false});
    }

    //update state onChange
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <section>
                <Link to={"/flashcards/" + this.state.set + "/" + this.state.topic}
                      className="test-link">Back to Set</Link>
                {this.state.finished ?
                    <div>
                        <button onClick={this.restart} className="test-link">Restart Test</button>
                    </div> : null
                }
                <div className="row">
                    <div className="column">
                        <form className="test-form" onSubmit={this.handleSubmit}>
                            <label name="front">{this.state.front}</label>
                            <input type="text" name="backinput" value={this.state.backinput}
                                   onChange={this.handleInputChange}/>
                            <input className="submit" type="submit" value="check" disabled={this.state.disabled}/>
                        </form>
                    </div>
                    <div className="column doughnut">
                        <Doughnut id="doughnut" data={this.syncDataset()}/>
                    </div>
                </div>
            </section>
        )
    }
}

export default withRouter(Testyourself);