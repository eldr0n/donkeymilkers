import React from "react";
import "./topic.css"
import {
    Link
} from "react-router-dom";


class Topic extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        };
    }

    render() {
        return (
            <Link to={"/flashcards/" + this.props.set + "/" + this.props.topic}>
                <div className="card-link">{this.props.topic}</div>
            </Link>
        );
    }
}

export default Topic;