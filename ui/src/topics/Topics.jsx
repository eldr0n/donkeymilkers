import React from "react";
import Topic from "./Topic.jsx"

class Topics extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            topics: []
        };
        this.passProps = this.passProps.bind(this);
    }

    //Get all topics
    componentDidMount() {
        fetch('/api/topics/' + this.props.set)
            .then(res => {
                return res.json();
            })
            .then(data => {
                this.setState({
                    topics: data
                });
            });
    }

    passProps(topic) {
        return (
            <Topic key={topic} set={this.props.set} topic={topic}/>
        );
    }

    render() {
        return (
            <section>
                {
                    this.state.topics.length > 0 ?
                        this.state.topics.map(this.passProps) : null
                }
            </section>
        );
    }
}

export default Topics;