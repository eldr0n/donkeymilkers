import React from "react";
import {toast} from 'react-toastify';

class Edit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            category: "",
            topic: "",
            front: "",
            back: ""
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    //load data of flashCard with given id
    componentDidMount() {
        fetch('/api/flashcards/' + this.state.id)
            .then(res => {
                return res.json();
            })
            .then(data => {
                console.log(data);
                this.setState({category: data.category});
                this.setState({topic: data.topic});
                this.setState({front: data.front});
                this.setState({back: data.back});

            });
    }

    //Post the updated flasCard to the database
    handleSubmit(event) {
        event.preventDefault();

        //create the needed JSON format
        let body = {
            id: this.state.id,
            topic: this.state.topic,
            category: this.state.category,
            front: this.state.front,
            back: this.state.back
        };

        //validation: everything filled?
        if (body.topic.length === 0 || body.category.length === 0 || body.front.length === 0 || body.back.length === 0) {
            console.log(body.back.length)
            toast.warn("Please fill in everything", {
                position: "bottom-center",
            });
        } else {
            fetch('/api/flashcards/' + this.state.id, {
                method: 'put',
                body: JSON.stringify(body),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }

            }).then(response => {
                //response success / error
                if (response.status !== 200) {
                    toast.error('Looks like there was a problem', {
                        position: "bottom-center",
                    });
                } else {
                    toast('🦄 Changes on Flashcard saved', {
                        position: "bottom-center",
                    });

                    //link to previous page
                    this.props.history.push('/flashcards/' + this.state.category + '/' + this.state.topic);
                    return response.json();
                }
            })
        }
    }

    //update state onChange
    handleInputChange(event) {
        let target = event.target;
        let value = target.value;
        let name = target.name;

        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <section>
                <form className="input-form" onSubmit={this.handleSubmit}>
                    <h1>Edit Flashcard</h1>
                    <label>Category:</label>
                    <input type="text" name="category" value={this.state.category} onChange={this.handleInputChange}/>

                    <label>Topic: </label>
                    <input type="text" name="topic" value={this.state.topic} onChange={this.handleInputChange}/>

                    <label>Front:</label>
                    <input type="text" name="front" value={this.state.front} onChange={this.handleInputChange}/>

                    <label>Back: </label>
                    <input type="text" name="back" value={this.state.back} onChange={this.handleInputChange}/>

                    <input className="submit" type="submit" value="Save Changes"/>
                </form>
            </section>

        )
    }
}

export default Edit;

