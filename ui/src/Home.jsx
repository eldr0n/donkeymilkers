import React from "react";
import "./index.css";
import donkeymilkers from './donkeymilkers.svg';

class Home extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }

    render(){
        return(
            <section>
                <img src={donkeymilkers} alt="Donkey-Logo"/>
                <h1>Welcome to Donkeymilkers!</h1>
                <h2>Create your own flash cards and improve your learning success.</h2>
            </section>
        )
    }
}
export default Home;